#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <stdarg.h>
#include <iostream>
#include <signal.h>
#include <getopt.h>
#include <curand_kernel.h>
#include "helper_cuda.h"
using namespace std;
#include "cudamacro.h"
#include "utils.h"
#define C 3
#define NTHREADS 64
#define REAL double
#define BASETYPE int

// FOR GRAPHS WITH <= 256 NODES
//#define TYPE unsigned char

// FOR GRAPHS WITH > 256 NODES
#define TYPE unsigned short

#define STYPE signed BASETYPE
#define NTYPE unsigned int
#define LTYPE unsigned long long int
#define FNORM   (2.3283064365e-10)
#define RANDOM  ((_ira[_ip++] = _ira[_ip1++] + _ira[_ip2++]) ^ _ira[_ip3++])
#define FRANDOM (FNORM * RANDOM)
#define OoZ (FRANDOM > 0.5)
#define NORMRAN (1.0-1.e-14)
#define PREFACTOR 0.096
#define SAMPLESxCORE 2
#define BITXBYTE 8

#define NARG 9
#define INVALIDWEIGTH -1.0
#define DEFNITER 10000 

#define MAXN 512
#define ITERBEC 100

#define NTH 128
#define TXS 32

/* variabili globali per il generatore random */
unsigned int myrand, _ira[256];
unsigned char _ip, _ip1, _ip2, _ip3;
enum {mult=16807, mod=2147483647};
NTYPE N, M;
TYPE *deg, **neigh, *graph, *unsat, *whereis;
TYPE *num, **list;
STYPE *s, *msc;
unsigned long long iter=0;
char hostname[HOST_NAME_MAX+1]; 
#define USECURAND
#if defined(USECURAND)
#define RANDSTATETYPE curandState
//#define RANDSTATETYPE curandStatePhilox4_32_10_t
#else
#define RANDSTATETYPE unsigned long long
#endif

void byebye(int dummy) {
     fprintf(stderr,"Node %s Process %d Restart value at the end: %lld\n",hostname,getpid(),iter);
}

void Usage(char *ProgName) {
    fprintf(stderr, "Usage: %s --N NumberOfSpin\n" 
		    "          --w1 weight1\n"
		    "          --w2 weight2\n"
		    "          --w3 weight3\n"	    	    
        	    "          [--Niter number_of_attempts]\n"
		    "          [--NumSamples number_of_multispin_coded_clones, 0 means auto selection based on number of GPU cores]\n"
		    "          [--Maxtime max_time_in_seconds]\n"
		    "          [--MaxRestart max_number_of_restart]\n"
		    "          [--RandomSeed random_seed]\n"
		    "          [--InstanceFile random_graph_file]\n"
		    "          [--Dumpfile dump_file_spin_and_random_numbers]\n"
		    "          [--Restartfile restart_file_spin_and_random_numbers]\n"
		    "          -help\n", ProgName);
    fprintf(stderr, "maxiter MUST be > 0 for single run and restart MUST be zero for single run\n");
    exit(EXIT_FAILURE);
}


void **mmcuda(void ***rp, int, int, int, int);
#define MAXSTRLEN 1024
void writelog(int end, int rc, const char *fmt, ...) {
  FILE *filelog=stderr;
  char buf[MAXSTRLEN+1];
  va_list ap;
  va_start(ap, fmt);
#ifdef  HAVE_VSNPRINTF
  vsnprintf(buf, MAXSTRLEN, fmt, ap);       /* safe */
#else
  vsprintf(buf, fmt, ap);                   /* not safe */
#endif
  fputs(buf,filelog);
  if(end) {
    fflush(filelog);
    fclose(filelog);
    exit(rc);
  }
}

unsigned int randForInit(void) {
  unsigned long long int y;
  
  y = myrand * 16807LL;
  myrand = (y & 0x7FFFFFFF) + (y >> 31);
  myrand = (myrand & 0x7FFFFFFF) + (myrand >> 31);
  return myrand;
}

void initRandom(void) {
   int i;
  
  _ip = 128;
  _ip1 = _ip - 24;    
  _ip2 = _ip - 55;    
  _ip3 = _ip - 61;
  
  for (i = _ip3; i < _ip; i++) {
    _ira[i] = randForInit();
  }
}

void error(const char *string) {
  fprintf(stderr, "ERROR: %s\n", string);
  exit(EXIT_FAILURE);
}

#if defined(USECURAND)  
__global__ void initRand(unsigned int seed, /*curandState*/RANDSTATETYPE *state, unsigned int n){
#else
__global__ void initRand(int seed, unsigned long long *state, unsigned int n) {
#endif

  const unsigned int tid = threadIdx.x + blockIdx.x * blockDim.x;

  /* Each thread gets same seed , a different sequence number  -  no offset */
  if(tid<n) {
#if defined(USECURAND)  
    curand_init (seed , tid , 0 , &state [ tid ]) ;
#else    
    state[tid]=(seed*(tid+1)*mult)%mod;
#endif    
  }

}

#if !defined(USECURAND)
#define GETRAN(s,r) (s)=(mult * (s)) % mod;\
                       (r)=int((s));
#endif

#define ENEX(X,Y,Z)	Ene0=C-(((X)&1)       +  ((Y)&1)     + ((Z)&1)    )    ; \
			Ene1=C-((((X)>>1)&1)  + (((Y)>>1)&1) + (((Z)>>1)&1))    ; \
			Ene2=C-((((X)>>2)&1)  + (((Y)>>2)&1) + (((Z)>>2)&1))    ; \
			Ene3=C-((((X)>>3)&1)  + (((Y)>>3)&1) + (((Z)>>3)&1))    ; \
			Ene4=C-((((X)>>4)&1)  + (((Y)>>4)&1) + (((Z)>>4)&1))    ; \
			Ene5=C-((((X)>>5)&1)  + (((Y)>>5)&1) + (((Z)>>5)&1))    ; \
		 	Ene6=C-((((X)>>6)&1)  + (((Y)>>6)&1) + (((Z)>>6)&1))    ; \
			Ene7=C-((((X)>>7)&1)  + (((Y)>>7)&1) + (((Z)>>7)&1))    ; \
			Ene8=C-((((X)>>8)&1)  + (((Y)>>8)&1) + (((Z)>>8)&1))    ; \
			Ene9=C-((((X)>>9)&1)  + (((Y)>>9)&1) + (((Z)>>9)&1))    ; \
			Ene10=C-((((X)>>10)&1)+ (((Y)>>10)&1)+ (((Z)>>10)&1))   ; \
			Ene11=C-((((X)>>11)&1)+ (((Y)>>11)&1)+ (((Z)>>11)&1))   ; \
			Ene12=C-((((X)>>12)&1)+ (((Y)>>12)&1)+ (((Z)>>12)&1))   ; \
			Ene13=C-((((X)>>13)&1)+ (((Y)>>13)&1)+ (((Z)>>13)&1))   ; \
			Ene14=C-((((X)>>14)&1)+ (((Y)>>14)&1)+ (((Z)>>14)&1))   ; \
			Ene15=C-((((X)>>15)&1)+ (((Y)>>15)&1)+ (((Z)>>15)&1))   ;

template<int X>
struct STATIC_LOG2 {
	enum {value = 1+STATIC_LOG2<X/2>::value};
};

template<>
struct STATIC_LOG2<1> {
	enum {value = 1};
};

template<int X>
struct STATIC_POW2 {
	enum {value = 1 << STATIC_LOG2<X>::value};
};

template<typename T>
union __align__(STATIC_POW2<6*sizeof(T)>::value) __byte_t {
	T v[STATIC_POW2<6*sizeof(T)>::value / sizeof(T)];
}__attribute__((__packed__));

enum {SRC_PRM = 6, DST_PRM = 7};

// NVERT MULTIPLE OF BDIM_X
// BDIM_X >= ncomp
template<int BDIM_X,
	 int BDIM_Y,
	 int NVERT>
__global__ 
void runsat_k(const TYPE deg[],
	      const TYPE *__restrict__ const *__restrict__ neigh,
	      STYPE *__restrict__ gs,
	      LTYPE *__restrict__ ta, 
	      RANDSTATETYPE *__restrict__ state,
	      const unsigned int numSamples,
	      const REAL w1,
	      const REAL w2,
	      const REAL w3,
	      volatile unsigned int *__restrict__ done,
	      NTYPE *__restrict__ totnum,
	      const TYPE ncomp,
	      const TYPE *__restrict__ compLen,
	      const TYPE *__restrict__ compPerm) {

	const int tidx = threadIdx.x;
	const int tidy = threadIdx.y;
	const int tid = tidy*BDIM_X + tidx;
	const int wid = blockIdx.x*BDIM_Y + tidy;

	const int lid = tid % 32;
	const unsigned int WMASK = ((1ull << BDIM_X)-1) << (lid & (~(BDIM_X-1)));

	LTYPE t=ta[wid];

	REAL ran;

	//const int ELXTH = NVERT/BDIM_X;
	
	__shared__ STYPE s_s[BDIM_Y][NVERT];

	__shared__ TYPE s_compPerm[NVERT];
	__shared__ TYPE s_destPerm[NVERT];
	__shared__ __byte_t<TYPE> s_neigh[NVERT];

	const int myCompLen = (tidx < ncomp) ? compLen[tidx] : 0;

	#pragma unroll
	for(int i = 0; i < NVERT; i += BDIM_X*BDIM_Y) {

		if (i+tid < NVERT) {
			const TYPE __src = compPerm[i+tid];

			s_compPerm[i+tid] = __src;
			s_destPerm[__src] = i+tid;
		}
	}
	__syncthreads();

	#pragma unroll
	for(int i = 0; i < NVERT; i += BDIM_Y) {
		if (i+tidy < NVERT && tidx < 6) {
			const TYPE myVert = s_compPerm[i+tidy];
			s_neigh[i+tidy].v[tidx] = s_destPerm[neigh[myVert][tidx]];
		}
	}

	__syncthreads();

	if(wid >= numSamples) {
		return;
	}

	RANDSTATETYPE localState=state[blockIdx.x*BDIM_X*BDIM_Y + tid];

	unsigned int niter=done[0];
	unsigned int TotEne;
	
	unsigned BASETYPE loop;
	
	#pragma unroll
	for(int i = 0; i < NVERT; i += BDIM_X) {
		if (i+tidx < NVERT) {
			const TYPE myVert = s_compPerm[i+tidx];
			s_s[tidy][i+tidx] = gs[myVert*numSamples + wid];
		}
	}
	__syncwarp(WMASK);

	do {
		for(int k = 0; k < ITERBEC-1; k++) { 

			int coff = 0;
			for(int i = 0; i < ncomp; i++) {

				const int clen = __shfl_sync(WMASK, myCompLen, i, BDIM_X); 

				if (tidx < clen) {
					const TYPE myVert = coff+tidx;
					const STYPE mySS = s_s[tidy][myVert];

					const __byte_t<TYPE> myNeigh = s_neigh[myVert];

					const STYPE xorr  = mySS ^ s_s[tidy][myNeigh.v[0]] ^ s_s[tidy][myNeigh.v[1]];
					const STYPE xorr2 = mySS ^ s_s[tidy][myNeigh.v[2]] ^ s_s[tidy][myNeigh.v[3]];
					const STYPE xorr3 = mySS ^ s_s[tidy][myNeigh.v[4]] ^ s_s[tidy][myNeigh.v[5]];

					const STYPE xorrr =   xorr ^ xorr2 ^ xorr3;
					const STYPE norr  = ~(xorr | xorr2 | xorr3);
					const STYPE nandr = ~(xorr & xorr2 & xorr3);

					ran = curand_uniform(&localState);				
					const STYPE xorrn = norr | (xorrr & nandr) | ((~xorrr) * (ran < w1));

					s_s[tidy][myVert] = mySS ^ xorrn;
				}
				coff += clen;
				__syncwarp(WMASK);
			}
		}

		loop = 0;
		int coff = 0;
		for(int i = 0; i < ncomp; i++) {

			const int clen = __shfl_sync(WMASK, myCompLen, i, BDIM_X); 

			if (tidx < clen) {
				const TYPE myVert = coff+tidx;
				const STYPE mySS = s_s[tidy][myVert];

				const __byte_t<TYPE> myNeigh = s_neigh[myVert];

				const STYPE xorr  = mySS ^ s_s[tidy][myNeigh.v[0]] ^ s_s[tidy][myNeigh.v[1]];
				const STYPE xorr2 = mySS ^ s_s[tidy][myNeigh.v[2]] ^ s_s[tidy][myNeigh.v[3]];
				const STYPE xorr3 = mySS ^ s_s[tidy][myNeigh.v[4]] ^ s_s[tidy][myNeigh.v[5]];

				const STYPE xorrr =   xorr ^ xorr2 ^ xorr3;
				const STYPE norr  = ~(xorr | xorr2 | xorr3);
				const STYPE nandr = ~(xorr & xorr2 & xorr3);

				ran = curand_uniform(&localState);				
				const STYPE xorrn = norr | (xorrr & nandr) | ((~xorrr) * (ran < w1));

				s_s[tidy][myVert] = mySS ^ xorrn;

				loop |= nandr;

			}
			coff += clen;
			__syncwarp(WMASK);
		}

		#pragma unroll
		for(int i = BDIM_X/2; i; i /= 2) {
			loop |= __shfl_xor_sync(WMASK, loop, i, BDIM_X);
		}

		t += ITERBEC;
		niter -= ITERBEC;
		TotEne = (loop==totnum[wid]);

	} while(TotEne && niter>0);
	
	state[blockIdx.x*BDIM_X*BDIM_Y + tid] = localState;
	__syncwarp(WMASK);

	#pragma unroll
	for(int i = 0; i < NVERT; i += BDIM_X) {
		if (i+tidx < NVERT) {

			const TYPE myVert = s_compPerm[i+tidx];
			gs[myVert*numSamples + wid] = s_s[tidy][i+tidx];
		}
	}
	if (tidx == 0) {
		totnum[wid]=loop;
		ta[wid]=t;
	}

	return;
}

void initRRGraph(char *filename) {
  unsigned int i, j, j1, j2, j3, index, changes, tmp;
  
  if(filename==NULL) {
  for (i = 0; i < M; i++)
    for (j = 0; j < 3; j++)
      graph[3*i+j] = i;
  do {
    changes = 0;
    for (i = 0; i < M; i++) {
      if (graph[3*i] == graph[3*i+1] ||
	  graph[3*i] == graph[3*i+2] ||
	  graph[3*i+1] == graph[3*i+2]) {
	index = (int)(FRANDOM * 3 * N);
	tmp = graph[index];
	graph[index] = graph[3*i];
	graph[3*i] = tmp;
	index = (int)(FRANDOM * 3 * N);
	tmp = graph[index];
	graph[index] = graph[3*i+1];
	graph[3*i+1] = tmp;
	index = (int)(FRANDOM * 3 * N);
	tmp = graph[index];
	graph[index] = graph[3*i+2];
	graph[3*i+2] = tmp;
	changes++;
      }
    }
  } while(changes);
  } else {
    FILE *fg=Fopen(filename,"r");
    char buffer[MAXSTRLEN];
    unsigned int a, b, c;
    for(i=0; i<N; i++){
      if(NULL==fgets(buffer,sizeof(buffer)-1,fg)) {
          writelog(1,1,"Error reading line %d from file %s\n",i,filename);
      }
      sscanf(buffer,"%d %d %d",&a,&b,&c);
      a--; b--; c--;
	  graph[3*i]=(TYPE)a;
	  graph[3*i+1]=(TYPE)b;
	  graph[3*i+2]=(TYPE)c;
    }
    fclose(fg);
  }
  for (i = 0; i < N; i++)
    deg[i] = 0;
  for (i = 0; i < M; i++) {
    j1 = graph[3*i];
    j2 = graph[3*i+1];
    j3 = graph[3*i+2];
    neigh[j1][2*deg[j1]] = j2;
    neigh[j1][2*deg[j1]+1] = j3;
    neigh[j2][2*deg[j2]] = j1;
    neigh[j2][2*deg[j2]+1] = j3;
    neigh[j3][2*deg[j3]] = j1;
    neigh[j3][2*deg[j3]+1] = j2;
    deg[j1]++;
    deg[j2]++;
    deg[j3]++;
  }
#if 0
  for (i = 0; i < M; i++) {
	  printf("neigh[%3d]:", i);
	  for(j = 0; j < 6; j++) {
		printf(" %d", neigh[i][j]);
	  }
	  printf("\n");
  }
#endif
}

void initSpin(void) {
  unsigned int i, j;

  for (i = 0; i < N; i++) {
    s[i] = OoZ;
    for(j=1; j<(sizeof(BASETYPE)/sizeof(char))*BITXBYTE; j++) {
        s[i] |= (OoZ<<j);
    }
  }
}

void fillLists(void) {
  unsigned int i, j;
  int sum;

  for (i = 0; i < 4; i++) { num[i] = 0; }
  for (i = 0; i < N; i++) {
    sum = 0;
    for (j = 0; j < deg[i]; j++) {
       sum += ((2*(s[neigh[i][2*j]]==s[neigh[i][2*j+1]]))-1);
    }
    unsat[i] = (C - ((2*s[i]-1) * sum)) / 2;
//    unsat[i] = (C - s[i] * sum) / 2;
    list[unsat[i]][num[unsat[i]]] = i;
    whereis[i] = num[unsat[i]]++;
  }
}


int numUnsat(void) {
  unsigned int i, res=0;
  

  for (i = 0; i < M; i++) {
    if (s[graph[3*i]] * s[graph[3*i+1]] * s[graph[3*i+2]] < 0) {
        res++;
    }
  }
  return res;
}

// quick and dirty independent set finder
TYPE findIndepSet(const int blockSize,
		  const int numVert,
		  const int numNeigh,
		  const TYPE *const *neigh,
		        TYPE *compLen,
			TYPE *compPerm) {

	TYPE ncomp = 0;
	int nadd = 0;

	TYPE *used = (TYPE *)Malloc(sizeof(*used)*numVert);
	memset(used, 0, sizeof(*used)*numVert);

	while(nadd < numVert) {
		int i;
		for(i = 0; i < numVert; i++) {
			if (!used[i]) {
				break;
			}
		}

		used[i] = 1;
		compLen[ncomp] = 1;
		compPerm[nadd++] = i;

		int toFind = blockSize-1;

		for(int j = 0; j < numVert; j++) {
			if (used[j]) {
				continue;
			}
	
			// check whether j is reached from
		        // the vertices in the current block
			int curBlockToJ = 0;
			for(int k = nadd-compLen[ncomp]; k < nadd; k++) {
				const int vert = compPerm[k];

				for(int l = 0; l < numNeigh; l++) {
					if (neigh[vert][l] == j) {
						curBlockToJ = 1;
						break;
					}
				}
			}
			if (curBlockToJ) {
				continue;
			}
			// check whether the vertices in
			// current block are reached from j
			int jToCurBlock = 0;
			for(int k = nadd-compLen[ncomp]; k < nadd; k++) {
				const int vert = compPerm[k];

				for(int l = 0; l < numNeigh; l++) {
					if (neigh[j][l] == vert) {
						jToCurBlock = 1;
						break;
					}
				}
			}
			if (jToCurBlock) {
				continue;
			}

			compPerm[nadd++] = j;
			compLen[ncomp]++;
			used[j] = 1;
			toFind--;
			if (!toFind) {
				break;
			}
		}
		ncomp++;
	}

	free(used);
	return ncomp;
}

int main(int argc, char *argv[]) {
  unsigned int i, is, numSamples=0, niter=DEFNITER;
  LTYPE *t;
  double w1=INVALIDWEIGTH, w2=INVALIDWEIGTH, w3=INVALIDWEIGTH;

  TYPE *d_deg, **d_neigh, *d_graph;
  NTYPE *d_totnum;
  TYPE **h_neigh;
  STYPE *d_s;
  LTYPE *d_t;
  unsigned int *d_done;
  NTYPE *h_totnum, *c_totnum;
  char *pathtodump=NULL;
  FILE *fp2dump=NULL;
  unsigned long long maxrestart=0;
  char *InstanceFile=NULL;
  char *OutFile=NULL;  

  RANDSTATETYPE *h_randstates;

   gethostname(hostname, HOST_NAME_MAX+1);

#if !defined(BASETYPE)
  writelog(1,1,"BASETYPE must be defined! It can be either char or short\n");
#endif

  RANDSTATETYPE  *randstates;

  cudaDeviceProp prop;
  cudaGetDeviceProperties(&prop, 0);
  NTYPE maskene; 
  if(sizeof(BASETYPE)==sizeof(char)) {
     maskene=0xFF;
  } else if(sizeof(BASETYPE)==sizeof(short)) {
     maskene=0xFFFF;
  } else if(sizeof(BASETYPE)==sizeof(int)) {
     maskene=0xFFFFFFFF;
  } else {
    writelog(1,1,"Invalid BASETYPE: %d\n",sizeof(BASETYPE));
  }
  
  FILE *devran = Fopen("/dev/random","r");
  //FILE *devran = fopen("/dev/urandom","r"); // lower quality
  const size_t keep_gcc_silent = Fread(&myrand, 4, 1, devran);
  fclose(devran);

  unsigned int ncore=_ConvertSMVer2Cores(prop.major, prop.minor) * prop.multiProcessorCount;
  numSamples=ncore*SAMPLESxCORE;

  unsigned long long maxiter;
  
  int maxtime=0;  
  int verbose=0;

  int och;
  while(1) {
		int option_index = 0;
		static struct option long_options[] = {
			{           "N", required_argument, 0, 'n'},
			{          "w1", required_argument, 0, 'x'},
			{          "w2", required_argument, 0, 'y'},
			{          "w3", required_argument, 0, 'w'},
			{       "Niter", required_argument, 0, 't'},			
			{  "NumSamples", required_argument, 0, 's'},
			{     "Maxtime", required_argument, 0, 'm'},
			{  "MaxRestart", required_argument, 0, 'g'},
			{  "RandomSeed", required_argument, 0, 'r'},
			{"InstanceFile", required_argument, 0, 'i'},			
			{    "Dumpfile", required_argument, 0, 'o'},
			{ "Restartfile", required_argument, 0, 'd'},									
			{     "verbose", required_argument, 0, 'v'},									
			{        "help",       no_argument, 0, 'h'},
			{             0,                 0, 0,   0}
		};

		och = getopt_long(argc, argv, "x:y:n:ohs:d:a:t:p:u:m:ecJ:r:v", long_options, &option_index);
		if (och == -1) break;
		switch (och) {
			case   0:// handles long opts with non-NULL flag field
				break;
			case 'n':
				N = atoi(optarg);
				maxiter=3*exp(PREFACTOR*N);
				break;
			case 'g':
			        maxrestart = atoll(optarg);			  
				break;
			case 'x':
				w1 = atof(optarg);
				break;
			case 'y':
				w2 = atof(optarg);
				break;
			case 'w':
				w3 = atof(optarg);
				break;
			case 'm':
				maxtime = atoi(optarg);
				break;
			case 'o':
			        OutFile=strdup(optarg);
				if(OutFile==NULL) {
				  fprintf(stderr,"Could not get memory for OutFile name\n");
				  exit(1);
				}
				break;
			case 'i':
			        InstanceFile=strdup(optarg);
				if(InstanceFile==NULL) {
				  fprintf(stderr,"Could not get memory for InstanceFile name\n");
				  exit(1);
				}
				break;
			case 'd':
			        pathtodump=strdup(optarg);
				if(pathtodump==NULL) {
				  fprintf(stderr,"Could not get memory for RestartFile name\n");
				  exit(1);
				}
				break;
			case 'h':
				Usage(argv[0]);
				break;
			case 'r':
				myrand = atoll(optarg);
				break;
			case 's':
				numSamples = atol(optarg);
				break;
			case 't':
				niter = atof(optarg);
				break;
			case 'v':
				verbose = 1;
				break;
			case '?':
			        Usage(argv[0]);
				exit(EXIT_FAILURE);
			default:
				fprintf(stderr, "unknown option: %c\n", och);
				exit(EXIT_FAILURE);
		}
  }
  if(N==0 || w1==INVALIDWEIGTH || w1==INVALIDWEIGTH || w1==INVALIDWEIGTH) {
    fprintf(stderr,"N=%d\n, w1=%f\n, w2=%f, w3=%f\n",N,w1,w2,w3);
    Usage(argv[0]);
  }
#if 0
  if (!InstanceFile) {
    fprintf(stderr, "Please spicify an instance file!\n");
    Usage(argv[0]);
  }
#endif
  M = N;

  cudaDeviceProp props;

  int devId = 0;
  CHECK_CUDA(cudaGetDevice(&devId));
  printf("\nUsing GPU:\n");
  CHECK_CUDA(cudaGetDeviceProperties(&props, devId));
  printf("\t%2d (%s, %d SMs, %d total cores)\n",
		  devId, props.name, props.multiProcessorCount, ncore);
  printf("\n");

  //fprintf(stderr,"Total number of cores=%d. Expected #iter=%llu\n", ncore, maxiter);
  //fprintf(stderr, "# 3spinRRG_time2gs.c  N = %d  M = %d  seed = %u pid=%d, maxiter=%llu, maxtime=%d\n", N, M, myrand,getpid(),maxiter,maxtime);
  //fprintf(stderr, "# C = %i  w1 = %g  w2 = %g  w3 = %g\n", C, w1, w2, w3);
  //fprintf(stderr, "# 1:N  2:time\n");
  printf("Run configuration:\n");
  printf("\t         N: %d\n", N);
  printf("\t         M: %d\n", M);
  printf("\t       pid: %d\n", getpid());
  printf("\t      seed: %u\n", myrand);
  printf("\t   maxiter: %llu\n", maxiter);
  printf("\t   maxtime: %d\n", maxtime);
  if(InstanceFile) {
	  printf("\tinst. file: %s\n", InstanceFile);
  }
  printf("\n");

  double __t0, __ft;
  s = (STYPE *)Malloc(N*sizeof(STYPE));
  msc = (STYPE *)Malloc(N*sizeof(STYPE));
  deg = (TYPE *)Malloc(N*sizeof(TYPE));
  neigh = (TYPE **)Malloc(N*sizeof(TYPE *));
  for (i = 0; i < N; i++) {
    neigh[i] = (TYPE *)Malloc(6*sizeof(TYPE));
  }  
  graph = (TYPE *)Malloc(3*M*sizeof(TYPE));
  h_totnum=(NTYPE *)Malloc(sizeof(NTYPE)*numSamples);
  c_totnum=(NTYPE *)Malloc(sizeof(NTYPE)*numSamples);
  for(is=0; is<numSamples; is++) {c_totnum[is]=h_totnum[is]=maskene;} 
  initRandom();
  initRRGraph(InstanceFile);

//  for (i = 0; i < N; i++) {
//    for(int j=0; j<6; j++) {
//        fprintf(stderr,"%d -- %d\n",i, neigh[i][j]);
//    }
//  }    
  CHECK_CUDA(cudaMalloc(&d_deg,sizeof(TYPE)*N));
  CHECK_CUDA(cudaMalloc(&d_graph,sizeof(TYPE)*3*M));
  CHECK_CUDA(cudaMalloc(&d_s,sizeof(STYPE)*N*numSamples));
  CHECK_CUDA(cudaMalloc(&d_t,sizeof(LTYPE)*numSamples));
  CHECK_CUDA(cudaMalloc(&d_totnum,sizeof(NTYPE)*numSamples));
  CHECK_CUDA(cudaMalloc(&d_done,sizeof(*d_done)));
  t=(LTYPE *)Malloc(sizeof(LTYPE)*numSamples);
  h_neigh=(TYPE **)mmcuda((void ***)&d_neigh,N,6,sizeof(TYPE),1);
  for (i = 0; i < N; i++) {
      CHECK_CUDA( cudaMemcpy(h_neigh[i], neigh[i], 6*sizeof(TYPE), cudaMemcpyHostToDevice) );
  }
  CHECK_CUDA( cudaMemcpy(d_graph, graph, sizeof(TYPE)*3*M, cudaMemcpyHostToDevice) );
  CHECK_CUDA( cudaMemcpy(d_deg, deg,
                     N*sizeof(TYPE), cudaMemcpyHostToDevice) );
  CHECK_CUDA( cudaMemcpy(d_done, &niter, sizeof(*d_done), cudaMemcpyHostToDevice ) );  
  
  dim3 block(TXS, NTH/TXS);
  dim3 grid((numSamples+block.y-1)/block.y, 1, 1);

  TYPE ncomp;
  TYPE compLen[TXS];
  TYPE compPerm[MAXN];

  printf("Partitioning the graph in independent sets... ");

  double __tp = Wtime();
  ncomp = findIndepSet(TXS, N, 6, neigh, compLen, compPerm);
  if (ncomp > TXS) {
	fprintf(stderr, "Please modify kernel to adjust to no. of components > than TXS!\n");
	exit(EXIT_FAILURE);
  }
  __tp = Wtime()-__tp;
  printf("done in %lf secs\n", __tp);

  if (verbose) {
	  fprintf(stderr, "Independent sets:\n", ncomp);
	  int z = 0;
	  for(int i = 0; i < ncomp; i++) {
		fprintf(stderr, "\tcomp[%d] (len: %3d):", i, compLen[i]);
		for(int j = 0; j < compLen[i]; j++) {
			fprintf(stderr, " %3d", compPerm[z++]);
		}
		fprintf(stderr, "\n");
	  }
  }

  int coff = 0;
  for(int i = 0; i < ncomp; i++) {

	int clen = compLen[i];
	
	TYPE cmask[N];
	memset(cmask, 0, sizeof(TYPE)*N);
	for(int j = 0; j < clen; j++) {
		cmask[compPerm[coff+j]] = 1u;
	}
	//for(int j = 0; j < N; j++) {
	//	fprintf(stderr, "cmask[%d]: %u\n", j, cmask[j]);
	//}
	for(int j = 0; j < clen; j++) {

		TYPE v = compPerm[coff+j];

		int vToCurComp = -1;
		for(int k = 0; k < 6; k++) {
			//if (v == 44) {
			//	fprintf(stderr, "neigh[%d][%d]: %d (mask[%d]: %d)\n", v, k, neigh[v][k],   neigh[v][k], cmask[neigh[v][k]]);
			//}
			if (cmask[neigh[v][k]] == 1) {
				vToCurComp = k;
				break;
			}
		}
		if (vToCurComp > -1) {
			fprintf(stderr,
				"Vertex %d in component %d has %d-th neighbor (%d) inside the component!\n",
				v, i, vToCurComp, neigh[v][vToCurComp]);
			exit(EXIT_FAILURE);
		}
		
		int curCompToV = -1;
		for(int l = 0; l < clen; l++) {
			const int a = compPerm[coff+l];
			for(int k = 0; k < 6; k++) {
				if (neigh[a][k] == v) {
					curCompToV = a;
					break;
				}
			}
		}
		if (curCompToV > -1) {
			fprintf(stderr,
				"Vertex %d in component %d reaches current vertex (%d)!\n",
				curCompToV, i, v);
			exit(EXIT_FAILURE);
		}
	}
	coff += clen;
  }
  if (verbose) {
	printf("Independent set decomposition verified successfully\n\n");
  	printf("Launching kernel with conf: <<%d, (%d, %d)>>>\n", grid.x, block.x, block.y);
  }
  printf("\n");

  TYPE *compLen_d=NULL;
  TYPE *compPerm_d=NULL;
  
  CHECK_CUDA(cudaMalloc(&compLen_d,  sizeof(*compLen_d)*TXS));
  CHECK_CUDA(cudaMalloc(&compPerm_d, sizeof(*compPerm_d)*N));

  CHECK_CUDA(cudaMemcpy(compLen_d, compLen, sizeof(*compLen_d)*TXS, cudaMemcpyHostToDevice));
  CHECK_CUDA(cudaMemcpy(compPerm_d, compPerm, sizeof(*compPerm_d)*N, cudaMemcpyHostToDevice));
  
  CHECK_CUDA ( cudaMalloc (( void **)&randstates, numSamples*TXS*sizeof ( RANDSTATETYPE ) ));
  initRand<<<(numSamples*TXS + NTH-1)/NTH, NTH>>>(myrand,randstates,numSamples*TXS);

  if(pathtodump) { /* restore status of random number generator */
     fp2dump=Fopen(pathtodump,"r");
     h_randstates=(RANDSTATETYPE *)Malloc(sizeof(RANDSTATETYPE)*numSamples);
     fread(h_randstates,sizeof(RANDSTATETYPE),numSamples,fp2dump);
     CHECK_CUDA( cudaMemcpy(randstates, h_randstates, sizeof(RANDSTATETYPE)*numSamples, cudaMemcpyHostToDevice) );
     free(h_randstates);
  }
  //cudaFuncSetCacheConfig(runsat_k<TXS, NTH/TXS, 128>, cudaFuncCachePreferL1 );
  //cudaFuncSetCacheConfig(runsat_k<TXS, NTH/TXS, 256>, cudaFuncCachePreferL1 );
  //cudaFuncSetCacheConfig(runsat_k<TXS, NTH/TXS, 320>, cudaFuncCachePreferL1 );

  //CHECK_CUDA(cudaDeviceSetSharedMemConfig(cudaSharedMemBankSizeEightByte));

  unsigned long long counter=0;
  unsigned int h_loop=1;
  unsigned int restart=1;
  //signal(SIGTERM | SIGUSR2 , byebye);
  //signal(SIGUSR2 , byebye);
  signal(SIGINT , byebye);

  printf("Starting ground state search...\n\n");
  fflush(stdout);

  __ft=Wtime();
  while(restart && (maxrestart==0 || iter<maxrestart)) {
     for(is=0; is<numSamples; is++) {c_totnum[is]=h_totnum[is]=maskene;}
     CHECK_CUDA( cudaMemcpy(d_totnum, h_totnum, numSamples*sizeof(NTYPE), cudaMemcpyHostToDevice) );
     for (is = 0; is < numSamples; is++) {
	if(pathtodump==NULL) {
	     memset(s,0,sizeof(STYPE)*N);
	     initSpin();
        } else {
	     fread(s,sizeof(STYPE),N,fp2dump);
	}
	CHECK_CUDA( cudaMemcpy(d_s+(is*N), s, sizeof(STYPE)*N, cudaMemcpyHostToDevice) );
     }
     __t0=Wtime();
     iter++;
     if(pathtodump) {
     }
     CHECK_CUDA( cudaMemset(d_t, 0, numSamples*sizeof(LTYPE) ) );
     counter=0;
     h_loop=1;
     while((counter<maxiter) && h_loop) {
	switch(N) {
	     case 128:
		runsat_k<TXS, NTH/TXS, 128><<<grid, block>>>(d_deg, d_neigh, d_s, d_t,
							     randstates, numSamples, w1, w2, w3,
							     d_done, d_totnum, ncomp, compLen_d, compPerm_d);
		break;
	     case 256:
		runsat_k<TXS, NTH/TXS, 256><<<grid, block>>>(d_deg, d_neigh, d_s, d_t,
							     randstates, numSamples, w1, w2, w3,
							     d_done, d_totnum, ncomp, compLen_d, compPerm_d);
		break;
	     case 320:
		runsat_k<TXS, NTH/TXS, 320><<<grid, block>>>(d_deg, d_neigh, d_s, d_t,
							     randstates, numSamples, w1, w2, w3,
							     d_done, d_totnum, ncomp, compLen_d, compPerm_d);
		break;
	     default:
		fprintf(stderr, "Unsupported N (%d)\n", N);
		exit(EXIT_FAILURE);
	}

        CHECK_CUDA( cudaMemcpy(h_totnum, d_totnum, numSamples*sizeof(NTYPE), cudaMemcpyDeviceToHost) );
        for(i=0; i<numSamples; i++) {
		 if(h_totnum[i]!=c_totnum[i]) {
		    if(maxtime || maxiter<(unsigned long long)(3*exp(PREFACTOR*N))) {
		      CHECK_CUDA( cudaMemcpy(t, d_t, numSamples*sizeof(LTYPE), cudaMemcpyDeviceToHost) );
		      c_totnum[i]=h_totnum[i];
		      fprintf(stderr, "Node %s Process %d restart %lld %d %x %d %f %f\n",hostname,getpid(),iter,i,h_totnum[i],t[i],Wtime()-__t0,Wtime()-__ft);
		    } else {
		      h_loop=0; restart=0; break; 
		    }
		 }
        }
        counter+=niter;
	if(maxtime) {
		    if((Wtime()-__t0)>maxtime) { fprintf(stderr, "Out of time "); h_loop=0; restart=0; break; }
	}
     }
  }   
  CHECK_CUDA( cudaMemcpy(t, d_t, numSamples*sizeof(LTYPE), cudaMemcpyDeviceToHost) );

  fprintf(stderr, "Total time: %f ",Wtime()-__t0);
  LTYPE mint=((LTYPE)1)<<((sizeof(LTYPE)*BITXBYTE)-1);  
  unsigned int mintid=0;
  for (is = 0; is < numSamples; is++) { 
      if(h_totnum[is]!=maskene && t[is]<mint) {
         mint=t[is];
      	 mintid=is;
      }   
  }
  fprintf(stderr, "Process %d, N=%i (expected iter=%llu) #start=%llu testing thread=%d ", getpid(), N, maxiter, iter, mintid);
  if(maxtime==0) { fprintf(stderr, "#iter=%llu ",mint); }
  for(i=0; i<N; i++) {
  	CHECK_CUDA( cudaMemcpy(&msc[i],d_s+(mintid+(i*numSamples)), sizeof(STYPE), cudaMemcpyDeviceToHost) );

  }
  fprintf(stderr, " NumUnsat ");
  for(i=0; i<N; i++) {
      s[i]=(msc[i]&1);
      if(s[i]==0) {s[i]=-1;}
  }
  fprintf(stderr, "(0)=%d ",numUnsat());
  for(unsigned int j=1; j<(sizeof(BASETYPE)/sizeof(char))*BITXBYTE; j++) {
    for(i=0; i<N; i++) {
      s[i]=(msc[i]>>j)&1;
      if(s[i]==0) {s[i]=-1;}
    }
    fprintf(stderr, "(%d)=%d ",j,numUnsat());
  }
  printf("\n");
  if(OutFile) { /* This is horrible! */
     FILE *fpout=Fopen(OutFile,"w");
     h_randstates=(RANDSTATETYPE *)Malloc(sizeof(RANDSTATETYPE)*numSamples);
     CHECK_CUDA( cudaMemcpy(h_randstates, randstates, sizeof(RANDSTATETYPE)*numSamples, cudaMemcpyDeviceToHost) );     
     Fwrite(h_randstates,sizeof(RANDSTATETYPE),numSamples,fpout);
     for (is = 0; is < numSamples; is++) {
	CHECK_CUDA( cudaMemcpy(s, d_s+(is*N), sizeof(STYPE)*N, cudaMemcpyDeviceToHost) );
	Fwrite(s,sizeof(STYPE),N,fpout);
     }
     fclose(fpout);
  }

  cudaFree(compLen_d);
  cudaFree(compPerm_d);

  if(pathtodump) { fclose(fp2dump); }
  if (InstanceFile) free(InstanceFile);
  return EXIT_SUCCESS;
}
