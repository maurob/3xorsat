CUDA_HOME=/usr/local/cuda
CUDACC=$(CUDA_HOME)/bin/nvcc
CC=gcc
LD=$(CUDACC)
CFLAGS=-c -O3 -I$(CUDA_HOME)/include
CUDACFLAGS=-O3 -lineinfo -arch=sm_70 -Xptxas=-v 
LDFLAGS=

all: 3xorsat

3xorsat: 3xorsat.o utils.o mmcuda.o
	$(LD) -o 3xorsat 3xorsat.o utils.o mmcuda.o $(LDFLAGS)

%.o: %.cu
	nvcc -c $(CUDACFLAGS) $<

%.o: %.c
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm *.o 3xorsat
