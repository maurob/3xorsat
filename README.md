To compile the code check the architecture of your GPU and set it accordingly
in Makefile (default architecture is Volta: sm_70). Then run ``make`` to build the ``3xorsat`` binary.

The code can be run specifying at least the following options:

<PRE>
./3xorsat --N NumberOfSpins --w1 weight1 --w2 weight2 --w3 weight3
</PRE>

where ``NumberOfSpins`` can be 128, 256 or 320. The three weights must be provided explicitly (see the paper for possible values).

There are several additional options:

<PRE>
 --Niter number_of_attempts
	max  number  of  attempts  before  restarting  from  a  different  initial  spin
	configuration.  By default maxiter=3*exp(0.096*N); (see paper for an explanation
	of the "magic" 0.096 value)

 --NumSamples number_of_multispin_coded_clones
	0 means auto selection based on number of cores available  on  the  GPU  in  use
	(default 0)

 --Maxtime max_time_in_seconds
	max elapsed time before restarting.  By default maxtime=0 meaning that there  is
	no time limit

 --MaxRestart max_number_of_restart
	max number of restart from a different initial spin configuration.   By  default
	maxrestart=0  meaning  that  there  is  no  limit  to  the  number  of   restart

 --RandomSeed random_seed
	by default the random seed is read from /dev/random

 --InstanceFile random_graph_file
	by default the graph is created by the program  but  it  can  be  read  from  an
	external file

 --Dumpfile dump_file_spin_and_random_numbers
	self explaining

 --Restartfile restart_file_spin_and_random_numbers
	self explaining
</PRE>

Example:

<PRE>
./3xorsat --N 128 --w1 0.055 --w2 1.0 --w3 1.0 --RandomSeed 12345

Using GPU:
	 0 (GeForce MX150, 3 SMs, 384 total cores)

Run configuration:
	         N: 128
	         M: 128
	       pid: 26778
	      seed: 12345
	   maxiter: 651226
	   maxtime: 0

Partitioning the graph in independent sets... done in 0.000101 secs

Starting ground state search...

Total time: 0.332256 Process 26778, N=128 (expected iter=651226) #start=1 testing thread=739 #iter=200  NumUnsat (0)=7 (1)=8 (2)=10 (3)=8 (4)=5 (5)=7 (6)=8 (7)=0 (8)=4 (9)=6 (10)=13 (11)=5 (12)=10 (13)=10 (14)=5 (15)=7 (16)=13 (17)=9 (18)=5 (19)=5 (20)=6 (21)=9 (22)=7 (23)=9 (24)=10 (25)=5 (26)=5 (27)=5 (28)=9 (29)=8 (30)=6 (31)=7
</PRE>

This ran on the GPU of a laptop. The run is successful when there is at least one instance that has 0 violated clauses. In this example is the instance 7.

